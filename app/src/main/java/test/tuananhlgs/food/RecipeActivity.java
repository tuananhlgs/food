package test.tuananhlgs.food;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import java.util.ArrayList;

public class RecipeActivity extends AppCompatActivity {

    ArrayList<Recipe> recipeList;
    ListView lvrecipe;
    RecipeAdapter adapter1;
    ActionBar actionBar;
    Toolbar tbRecipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        mapped();

        setSupportActionBar(tbRecipe);
//        tabLayout.addTab(tabLayout.newTab().setText("Recipes"));
//        tabLayout.addTab(tabLayout.newTab().setText("Wine"));
//        tabLayout.addTab(tabLayout.newTab().setText("Profile"));

//        setTitle("Recipe");
//        actionBar.setIcon(R.drawable.icon);

        adapter1 = new RecipeAdapter(this, R.layout.recipe_line, recipeList);
        lvrecipe.setAdapter(adapter1);
        adapter1.notifyDataSetChanged();
    }

    private void mapped() {
        lvrecipe = (ListView) findViewById(R.id.list_recipe);
        tbRecipe = (Toolbar) findViewById(R.id.toolbar_recipe);

        recipeList = new ArrayList<>();
        recipeList.add(new Recipe("Mandarian Sorbet", "by John Doe", R.drawable.img1, R.drawable.user1));
        recipeList.add(new Recipe("Blackberry Sorbet", "by Stansil Kirilov", R.drawable.img2, R.drawable.user1));
        recipeList.add(new Recipe("Mango Sorbet", "by John Doe", R.drawable.img3, R.drawable.user1));
        recipeList.add(new Recipe("Strawberry Sorbet", "by Steve Thomas", R.drawable.img4, R.drawable.user1));
        recipeList.add(new Recipe("Apple Sorbet", "by Mark Kratzman", R.drawable.img5, R.drawable.user1));
        recipeList.add(new Recipe("Sweet Sorbet", "by Selene Setty", R.drawable.img6, R.drawable.user1));
    }
}
