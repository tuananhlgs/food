package test.tuananhlgs.food;

/**
 * Created by DELL-PC on 7/13/2017.
 */

public class Recipe {
    private String foodName, chef;
    private int foodImage, userImage;

    public Recipe(String foodName, String chef, int foodImage, int userImage) {
        this.foodName = foodName;
        this.chef = chef;
        this.foodImage = foodImage;
        this.userImage = userImage;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getChef() {
        return chef;
    }

    public void setChef(String chef) {
        this.chef = chef;
    }

    public int getFoodImage() {
        return foodImage;
    }

    public void setFoodImage(int foodImage) {
        this.foodImage = foodImage;
    }

    public int getUserImage() {
        return userImage;
    }

    public void setUserImage(int userImage) {
        this.userImage = userImage;
    }
}
