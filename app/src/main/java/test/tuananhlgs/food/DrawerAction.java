package test.tuananhlgs.food;

/**
 * Created by DELL-PC on 7/13/2017.
 */

public class DrawerAction {

    String actionText;
    int iconImage;

    public DrawerAction(String actionText, int iconImage) {
        this.actionText = actionText;
        this.iconImage = iconImage;
    }

    public String getActionText() {
        return actionText;
    }

    public void setActionText(String actionText) {
        this.actionText = actionText;
    }

    public int getIconImage() {
        return iconImage;
    }

    public void setIconImage(int iconImage) {
        this.iconImage = iconImage;
    }
}
